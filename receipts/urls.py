from django.urls import path


from receipts.views import (
    ReceiptListView,
    AccountCreateView,
    ExpenseCategoryCreateView,
    ReceiptCreateView,
    AccountListView,
    ExpenseCategoryListView,
)


urlpatterns = [
    # path = a function that looks for ("") in the url, if it finds it
    # excutes the ().as_view), what we as the developer want to (name) it
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path("accounts/", AccountListView.as_view(), name="list_accounts"),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="create_account"
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="create_category",
    ),
    path("create/", ReceiptCreateView.as_view, name="create_receipt"),
]
