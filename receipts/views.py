from re import template
from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView

# Create your views here.
from django.contrib.auth.mixins import LoginRequiredMixin
from receipts.models import Account, ExpenseCategory, Receipt


# creating class that will show a list of receipt, it is inheriting
# from LoginRequiredMixin
class ReceiptListView(LoginRequiredMixin, ListView):
    # inherit from receipt model
    model = Receipt
    template_name = "receipts/list.html"

    # Need some sort of function here based on templateView
    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


# parent is createView, multiple inheritance
class AccountCreateView(LoginRequiredMixin, CreateView):
    # a variable that is making a reference to the Account class. not an
    # instance
    model = Account
    # a variable that is making a reference to the create.html
    template_name = "accounts/create.html"
    # these are my variables in my Account class
    fields = ["name", "number"]

    # this assigns the current user to a user property,
    # basically adds the information to the user's record/instance in the
    # database
    def form_valid(self, form):
        # getting the information
        item = form.save(commit=False)
        # assigning it to an a person
        item.owner = self.request.user
        # saving that instance to that owner
        item.save()
        # takes you back to the list
        return redirect("list_accounts")


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "expense_categories/create.html"
    fields = ["name"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("list_categories")


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accounts/list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "expense_categories/list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)
