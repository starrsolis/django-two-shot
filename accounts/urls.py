# we are importing the path function from the django database
from django.urls import path

# we are importing classes from the django database
from django.contrib.auth.views import LoginView, LogoutView

# importing the signup function from my view.py
from accounts.views import signup

urlpatterns = [
    # path = a function that looks for ("") in the url, if it finds it
    # excutes the ().as_view), what we as the developer want to (name) it
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("signup/", signup, name="signup"),
]
