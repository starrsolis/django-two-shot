from django.shortcuts import redirect, render
from django.contrib.auth import login

##importing the stuff for the create_user method
# https://docs.djangoproject.com/en/4.0/topics/auth/default/#creating-users
from django.contrib.auth.models import User

# importing the UserCreationFor from django so we can
# implement the ablity to sign up
from django.contrib.auth.forms import UserCreationForm

# create function to make a user
def signup(request):
    # post is a HTTP method, it is used to send data to the server
    # create or update the (database?, dictionary?)
    if request.method == "POST":
        form = UserCreationForm(request.Post)
        if form.is_valid():
            # this gets the username from the request.Post dictionary
            username = request.Post.get("username")
            # this gets the password from the request.Post dictionary
            password = request.Post.get("password1")
            # we need to create a new user object
            user = User.objects.create_user(
                # first username, password are defining the instance
                # for the create_user function and setting them equal
                # to the username variable established above.
                username=username,
                password=password,
            )
            # this saves the new user, got this info from the docs
            # https://docs.djangoproject.com/en/4.0/topics/auth/default/#creating-users
            user.save()
            # after we save, we want to be asked to log in and then
            # redirected back to the home page
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm()
    # create variable with an Object[Key/Value] relationship
    context = {"form": form}
    ## is this similar to when we create a path in urlpattterns?
    return render(request, "registration/signup.html", context)
